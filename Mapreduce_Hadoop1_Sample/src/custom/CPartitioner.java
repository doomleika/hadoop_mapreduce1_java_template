package custom;
import org.apache.hadoop.mapreduce.Partitioner;

// Modify the Partitioner<KEY, VALUE> to match your own mapreduce output
public class CPartitioner<K, V> extends Partitioner<K, V> {

	@Override
	public int getPartition(K key, V value, int numReduceTasks) {
		// Write your own partitioner here
		// Remember there's hadoop's own partitioner:
		// BinaryPartitioner, HashPartitioner, KeyFieldBasedPartitioner,
		// SecondarySort.FirstPartitioner, TotalOrderPartitioner
		// 
		// This directly copy from HashPartitioner
		return (key.hashCode() & Integer.MAX_VALUE) % numReduceTasks;
	}

}
