package custom.test;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;


import org.junit.Before;
import org.junit.Test;

import custom.CMapper;
import custom.CReducer;

public class CMapReduceTest {

	MapReduceDriver<LongWritable, Text, Text, Text, Text, Text> mapReduceDriver;
	
	@Before
	public void setUp() {
		CMapper mapper = new CMapper();
		CReducer reducer = new CReducer();
		this.mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	}

	@Test
	public void testMapReduceDriver() 
		throws IOException
	{
		LongWritable lw = new LongWritable(0);
		
		this.mapReduceDriver.withInput(lw, new Text("test1"));
		this.mapReduceDriver.withOutput(new Text("0"), new Text("test1"));
		
		this.mapReduceDriver.runTest();
	}
	
}
