package custom.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;


import org.junit.Before;
import org.junit.Test;

import custom.CReducer;

public class CReduceTest {

	ReduceDriver<Text, Text, Text, Text> reduceDriver;
	
	@Before
	public void setUp() {
		CReducer reducer = new CReducer();

		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}

	@Test
	public void testReducer() 
		throws IOException
	{
		List<Text> list = new ArrayList<Text>();
		list.add(new Text("test1"));
		this.reduceDriver.withInput(new Text("0"), list);
		this.reduceDriver.withOutput(new Text("0"), new Text("test1"));
		this.reduceDriver.runTest();
	}
	
}
