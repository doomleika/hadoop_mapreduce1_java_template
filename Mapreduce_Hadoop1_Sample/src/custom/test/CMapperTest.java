package custom.test;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;


import org.junit.Before;
import org.junit.Test;

import custom.CMapper;

public class CMapperTest {

	MapDriver<LongWritable, Text, Text, Text> mapDriver;
	
	@Before
	public void setUp() {
		CMapper mapper = new CMapper();

		mapDriver = MapDriver.newMapDriver(mapper);
	}

	@Test
	public void testMapper() 
		throws IOException
	{
		this.mapDriver.withInput(new LongWritable(0), new Text("test1"));
		this.mapDriver.withOutput(new Text("0"), new Text("test1"));
		this.mapDriver.runTest();
	}
	
}
