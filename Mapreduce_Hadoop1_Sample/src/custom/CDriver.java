package custom;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class CDriver extends Configured implements Tool{
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new CDriver(), args);
		System.exit(res);
	}
	
	@Override
	public int run(String[] args) throws Exception {
		
		if(args.length != 2) {
			System.out.println("<input> <output>");
			System.exit(1);
		}
		
		String input = args[0];
		String output = args[1];
		
		Configuration conf = this.getConf();
		Job job = new Job(conf);
		job.setJarByClass(CDriver.class);
		job.setJobName("MapReduceJobName: input: " + input + " output: " + output);
		
		// Default TextInputFormat, or
		// CombineFileInputFormat, KeyValueTextInputFormat, NLineInputFormat, SequenceFileInputFormat, TextInputFormat
		// DBInputFormat for DB read
		// DelegatingInputFormat
		FileInputFormat.setInputPaths(job, new Path(input));
        job.setInputFormatClass(TextInputFormat.class);
        
        // Number of maps are dictitated by the following parameter, check source for more detailed info
        // FileInputFormat.setMaxInputSplitSize(job, Long.MAX_VALUE);
        // FileInputFormat.setMinInputSplitSize(job, 0);
		
		// Set your custom mapper class here
		job.setMapperClass(CMapper.class);
		// Output Key/Value class has to match your own mapper's class 
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		
		// job.setCombinerClass(CCombiner.class);
		
		// Use this or use -Dmapred.reduce.tasks=<num> for taskcontrol
		// job.setNumReduceTasks(1);
		
		// In case you need custom partitioner
		//job.setPartitionerClass(CPartitioner.class);
		
		// Set your custom reducer class here
		job.setReducerClass(CReducer.class);
		// Output Key/Value class this has to match reducer output key/value type
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		

        // Output
        FileOutputFormat.setOutputPath(job, new Path(output));
        job.setOutputFormatClass(TextOutputFormat.class);
		
        // use job.submit() if you wish to monitor job yourself.
		return job.waitForCompletion(true) ? 0 : 1;
	}

}
