package custom;
import java.io.IOException;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;

// Check hadoop's impelementation of default Mappers before write your own!
// DelegatingMapper, FieldSelectionMapper, InverseMapper, MultithreadedMapper, SecondarySort.MapClass, TokenCounterMapper, WordCount.TokenizerMapper

public class CMapper extends Mapper<LongWritable, Text, Text, Text>{
	@Override
	protected void setup(Context context) 
		throws IOException, InterruptedException
	{
		// do your setup up code here
	}
	
	@Override
	protected void map(LongWritable key, Text value, Context context)
		throws IOException, InterruptedException
	{
		context.write(new Text(key.toString()), value);
	}
	
	@Override
	protected void cleanup(Context context)
		throws IOException, InterruptedException
	{
		// do your clean up code here
	}
	
	// custom run method, if you don't know what does this do: 
	//
	//	****DONT TOUCH THEM, AT ALL****
	//
	//	In short it call setup(Mapper.Context) at first,
	//	then invoke map(KEYIN, VALUEIN) for each key/value pair, 
	//	then invoke cleanup(Mapper.Context)
	//
	//	@Override
	//	public void run(Mapper.Context context)
	//			throws IOException, InterruptedException
	//	{
	//		super.run(context);
	//	}
	
}
