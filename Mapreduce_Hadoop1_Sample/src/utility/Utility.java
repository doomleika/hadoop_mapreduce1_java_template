package utility;
import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;


public class Utility {
	
	static public long getFileSize(Path path, Job job) 
			throws IllegalArgumentException, IOException
	{
		long result = getFileSize(path, job.getConfiguration());
		return 	result;
	}
	
	static public long getFileSize(Path path, Configuration conf) 
			throws IllegalArgumentException, IOException
	{
		FileSystem fs = path.getFileSystem(conf);
		return 	fs.getFileStatus(path).getLen();
	}

}
